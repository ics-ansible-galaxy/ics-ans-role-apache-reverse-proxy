ics-ans-role-apache-reverse-proxy
===================

Ansible role to install apache-reverse-proxy.
Sets both path and domain based reverse proxies.
Example (path based): http://domain.com/app1
Example (domain based): http://server1.domain.com

Requirements
------------

- ansible >= 2.4
- molecule >= 2.6
- src: git+https://gitlab.esss.lu.se/ics-ansible-galaxy/ics-ans-role-repository.git 
- Docker ce >= 18

Role Variables
--------------

```yaml
...
```

Example Playbook
----------------

```yaml
path_to_httpd_dir: /etc/httpd/conf.d
path_to_httpd_ssl: "{{ path_to_httpd_dir }}/ssl"
path_to_httpd_public: "{{ path_to_httpd_ssl }}/httpd.pem"
path_to_httpd_crt: "{{ path_to_httpd_ssl }}/httpd.crt"
path_to_httpd_csr: "{{ path_to_httpd_ssl }}/httpd.csr"
path_to_httpd_dh: "{{ path_to_httpd_ssl }}/dhparam.pem"
path_to_httpd_private: "{{ path_to_httpd_ssl }}/httpd.key"
force_certificates: false
httpd_country: SE
httpd_ou: Company Name
httpd_email: emailaddress@domain.com
httpd_san: 'DNS:*.domain.com,DNS:*.subdomain.domain.com'

httpd_proxy_file: ssl.conf

reverse_proxy_host:
  - frontend: virtualhost.domain.com
    backend: https://realserver.domain.com
  - frontend: virt1.domain.com
    backend: https://real1.domain.com
    backendport: 1234
...
```

License
-------

BSD 2-clause
---
